package com.example.exercice4.pojo;

public class Contact {
    private int id;
    private String lastname;
    private String firstname;
    private String phone;

    public Contact() {
    }

    public Contact(String lastname, String firstname, String phone) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.phone = phone;
    }

    public String getLastName() {
        return lastname;
    }

    public void setLastName(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstName() {
        return firstname;
    }

    public void setFirstName(String firstname) {
        this.firstname = firstname;
    }

    public String getPhoneNumber() {
        return phone;
    }

    public void setPhoneNumber(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return lastname + " " + firstname + " : " + phone;
    }
}
