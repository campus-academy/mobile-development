package com.example.exercice4;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.exercice4.pojo.Contact;

import java.util.List;

public class ContactActivity extends AppCompatActivity {
    private Button viewMainButton;
    private SQLiteDatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        db = new SQLiteDatabaseHandler(this);
        List<Contact> contacts = db.allContacts();

        if (contacts != null) {
            String[] itemsNames = new String[contacts.size()];

            for (int i = 0; i < contacts.size(); i++) {
                itemsNames[i] = contacts.get(i).toString();
                System.out.println(itemsNames[i]);
            }

            // display like string instances
            ListView list = (ListView) findViewById(R.id.result);
            list.setAdapter(new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, itemsNames));
        }
    }
}
