package com.example.exercice4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.exercice4.pojo.Contact;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private SQLiteDatabaseHandler db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new SQLiteDatabaseHandler(this);

        Button createButton = findViewById(R.id.createBtn);
        Button seeButton = findViewById(R.id.seeBtn);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText firstName = findViewById(R.id.firstname);
                EditText lastName = findViewById(R.id.lastname);
                EditText phone = findViewById(R.id.phone);
                String name = firstName.getText().toString() + " " + lastName.getText().toString();
                String phoneNumber = phone.getText().toString();

                Toast.makeText(MainActivity.this, "Contact " + name + " crée", Toast.LENGTH_SHORT).show();

                Contact contact = new Contact(firstName.getText().toString(), lastName.getText().toString(), phoneNumber);

                db.addContact(contact);
            }
        });

        seeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                startActivity(intent);
            }
        });
    }
}
