package com.example.exercice4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.exercice4.pojo.Contact;

import java.util.LinkedList;
import java.util.List;

public class SQLiteDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ExerciseDB";
    private static final String TABLE_NAME = "exerciseDb";
    private static final String KEY_ID = "id";
    private static final String KEY_FIRSTNAME = "firstName";
    private static final String KEY_LASTNAME = "lastName";
    private static final String KEY_PHONENUMBER = "phoneNumber";
    private static final String[] COLUMNS = { KEY_ID, KEY_FIRSTNAME, KEY_LASTNAME, KEY_PHONENUMBER };
    private static final String REQUETE_CREATION_DB = "create table " + TABLE_NAME + " (" +
            KEY_ID + " INTEGER primary key autoincrement, " +
            KEY_FIRSTNAME + " TEXT, " +
            KEY_LASTNAME + " TEXT, " +
            KEY_PHONENUMBER + " TEXT );";
    public static final String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private SQLiteDatabase maBaseDonne;

    public SQLiteDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(REQUETE_CREATION_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TABLE_DROP);
        onCreate(db);

    }

    public List<Contact> allContacts() {
        List<Contact> contacts = new LinkedList<Contact>();
        String query = "SELECT * FROM " + TABLE_NAME + ";";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Contact contact = null;

        if (cursor.moveToFirst()) {
            do {
                contact = new Contact();
                contact.setLastName(cursor.getString(1));
                contact.setFirstName(cursor.getString(2));
                contact.setPhoneNumber(cursor.getString(3));
                contacts.add(contact);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return contacts;

    }

    public void addContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_LASTNAME, contact.getLastName());
        values.put(KEY_FIRSTNAME, contact.getFirstName());
        values.put(KEY_PHONENUMBER, contact.getPhoneNumber());

        db.insert(TABLE_NAME,null, values);
        db.close();
    }
}
