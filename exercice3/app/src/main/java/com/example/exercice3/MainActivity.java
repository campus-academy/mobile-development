package com.example.exercice3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    private String readFromFile(Context context) {
        String ret = "";
        try {
            InputStream inputStream = context.openFileInput("contact_file.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append("\n").append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button createButton = findViewById(R.id.createBtn);
        Button seeButton = findViewById(R.id.seeBtn);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText firstName = findViewById(R.id.firstname);
                EditText lastName = findViewById(R.id.lastname);
                EditText phone = findViewById(R.id.phone);
                String name = firstName.getText().toString() + " " + lastName.getText().toString();
                String phoneNumber = phone.getText().toString();

                String contact = name + " : " + phoneNumber + "\n";

                Toast.makeText(MainActivity.this, "Contact " + name + " crée", Toast.LENGTH_SHORT).show();

                String FILENAME = "contact_file.txt";

                FileOutputStream fos = null;
                try {
                    fos = openFileOutput(FILENAME, MainActivity.MODE_APPEND);
                    fos.write(contact.getBytes());
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        seeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ContactActivity.class);
                String[] contacts = readFromFile(MainActivity.this).split("\n");
                intent.putExtra("contacts", contacts);
                startActivity(intent);
            }
        });
    }
}
