package com.example.exercice2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> contacts = new ArrayList<String>();
    private int count =+ 1;

    @Override
    public void onRestart() {
        super.onRestart();
        Intent intent=new Intent();
        intent.setClass(this, this.getClass());
        finish();
        this.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button createButton = findViewById(R.id.createBtn);
        Button seeButton = findViewById(R.id.seeBtn);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText firstName = findViewById(R.id.firstname);
                EditText lastName = findViewById(R.id.lastname);
                EditText phone = findViewById(R.id.phone);
                String name = firstName.getText().toString() + " " + lastName.getText().toString();
                String phoneNumber = phone.getText().toString();

                String contact = name + " : " + phoneNumber;

                contacts.add(contact);
                Toast.makeText(MainActivity.this, "Contact " + name + " crée", Toast.LENGTH_SHORT).show();
            }
        });

        seeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ContactActivity.class);
                intent.putExtra("contacts", contacts);
                startActivity(intent);
            }
        });

        TextView counter = findViewById(R.id.counter);
        counter.setText(Integer.toString(count));
    }

    protected void onSavedInstanceState(Bundle savedInstanceState) {
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
