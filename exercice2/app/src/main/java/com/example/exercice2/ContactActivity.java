package com.example.exercice2;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ContactActivity extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                getIntent().getStringArrayListExtra("contacts")
        );

        ListView listView = findViewById(R.id.result);
        listView.setAdapter(adapter);

    }
}
